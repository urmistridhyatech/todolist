import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StorageServiceModule } from  'ngx-webstorage-service';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import { HttpClientModule, HTTP_INTERCEPTORS}    from '@angular/common/http';
import { LoaderInterceptor } from './interceptors/loader-interceptor.service';
import { RegisterComponent } from './register/register.component';
import { TasklistComponent } from './tasklist/tasklist.component';
import { ParenttaskComponent } from './parenttask/parenttask.component';
import { LogoutComponent } from './logout/logout.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { StatusPipe } from './status.pipe';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    TasklistComponent,
    ParenttaskComponent,
    LogoutComponent,
    PagenotfoundComponent,
    StatusPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    StorageServiceModule,
    ToastrModule.forRoot(),// ToastrModule added
    FormsModule,
    NgxPaginationModule
  ],
  
    providers: [
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
