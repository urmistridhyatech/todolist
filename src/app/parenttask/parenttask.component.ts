import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-parenttask',
  templateUrl: './parenttask.component.html',
  styleUrls: ['./parenttask.component.css']
})
export class ParenttaskComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }
   displaying="none";
  isTrue:boolean =false;
 w3_open() {

  this.displaying="block";
  this.isTrue=true;
}
w3_close()
{
  this.displaying="none";

	this.isTrue=false;
}


}
