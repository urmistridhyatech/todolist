import { Component, OnInit } from '@angular/core';
import {RegisterService} from '../register.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.css']
})
export class LogoutComponent implements OnInit {

  constructor(private RegisterService:RegisterService,private router:Router) { }

  ngOnInit(): void {

    this.RegisterService.logout();
   	this.router.navigate(['']);
   	 }

}
