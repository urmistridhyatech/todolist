import { Injectable,Inject } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Observable, of } from 'rxjs';
import { HttpClient,HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
let tasks="tasks";
let data=[];
let isExist=false;
@Injectable({
  providedIn: 'root'
})
export class TaskService {

constructor(@Inject(LOCAL_STORAGE) private storage:StorageService,private toastr: ToastrService,private router:Router,private http: HttpClient) { }
private addTaskUrl="https://php2.shaligraminfotech.com/todo_management/public/api/addTask";
private getTaskUrl="https://php2.shaligraminfotech.com/todo_management/public/api/taskListing";
private StatusUrl="https://php2.shaligraminfotech.com/todo_management/public/api/complitedTask";
private updateTaskUrl="https://php2.shaligraminfotech.com/todo_management/public/api/updateTask";
private deleteTaskUrl="https://php2.shaligraminfotech.com/todo_management/public/api/deleteTask";
private id;

/*Addd task using localstorage
  newTask(taskData)
  {
  		const taskList = this.storage.get(tasks) || [];
          	taskList.push({
          		title:taskData.title,
              done:false
          	
              
          });
          this.storage.set(tasks, taskList);
  }*/

   newTask(taskData)
  {
   return this.http.post<any>(this.addTaskUrl,taskData)
  }
  taskDetails()
  {
      this.toastr.success('Succussfully Added');
      this.router.navigate(['tasks']); 
  }

  // getTasks()
  // {
  //        	data=this.storage.get(tasks);
    // return data;
    // }
  getTasks()
  {
   return this.http.get<any>(this.getTaskUrl)

  }
  /*update task using localstorage
  updateTask(title,id)
  {
     
        data=this.storage.get(tasks);
          console.log(data);
       
          data[id]['title']=title;
         
          this.storage.set(tasks,data);

         
        
  }*/
  updateTask(task_name,id)
  {
  const params = new HttpParams()
  .set('id', id)
  .set('task_name',task_name)
  return this.http.put<any>(this.updateTaskUrl+'?'+params.toString(),id)
    }

  deleteTask(id)
  {
  const params = new HttpParams()
  .set('id', id)
  return this.http.delete<any>(this.deleteTaskUrl+'?'+params.toString(),id)
 }
  updateError(data)
  {
    console.log(data)
    this.getTasks();
    this.toastr.success('Succussfully Updated');
    this.router.navigate(['tasks']); 
  }

  updateStatus(tasks)
  {
    return this.http.post<any>(this.StatusUrl,tasks)
    }
    /*update task to complted using local storage
   updateStatus(done,id)
  {

         data=this.storage.get(tasks);
         
          if(done==true)
          {
          data[id]['done']=false;
         }
         else
         {
          data[id]['done']=true;
         }

        this.storage.set(tasks,data);
        
         
        
  }
  deleteTask(id)
  {

        data=this.storage.get(tasks);
        console.log(data);
        data.splice(id,1);
        this.storage.set(tasks,data);

        console.log(data);
}*/
}
