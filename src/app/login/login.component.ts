import { Component, OnInit } from '@angular/core';
import {RegisterService} from '../register.service';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public RegisterService:RegisterService,private toastr: ToastrService,private router:Router) { }
  //isAuthenticate:boolean=false;
  message:string="";
  data=[];
  ngOnInit(): void {
  }
  userModel={
  
 "email":"",
 "password":""
 };


//excecutes when login form submited
 	onSubmit()
  {
   this.RegisterService.login(this.userModel)
         .subscribe(
          data=>this.RegisterService.authChecking(data),
  );
  }
       
   /* local storage 
   this.isAuthenticate=this.RegisterService.authCheck();
    if(this.isAuthenticate==true)
    {
    this.toastr.success('Succussfully logged In');

    this.router.navigate(['tasks']); 
    

	}
	else{
		this.message="wrong credentials please try again";

	}

*/
    
  
  
}
