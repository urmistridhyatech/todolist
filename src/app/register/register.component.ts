import { Component, OnInit } from '@angular/core';
import {RegisterService} from '../register.service';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
isEmailExit:boolean=false;
message:string="";
  constructor(private RegisterService:RegisterService,private toastr: ToastrService,private router:Router) { }

  ngOnInit(): void {
  }

  userModel={
  "firstname":"",
  "lastname":"",
 "email":"",
 "password":""
 };


onSubmit()
  {
    //console.log(this.userModel);
  this.isEmailExit=this.RegisterService.register(this.userModel);
   if(this.isEmailExit==false)
    {
    this.toastr.success('Succussfully Registered');
    this.router.navigate(['']); // tells them they've been logged out (somehow)

    }
    else
    {
      this.message="This Email already Exits";

    }
  }
}
