import { Injectable,Inject } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { RegisterService } from './register.service';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
let token;


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {


  isAuthenticate:boolean;

constructor(private RegisterService:RegisterService,@Inject(LOCAL_STORAGE) private storage:StorageService,private toastr: ToastrService,private router:Router){}
  canActivate()
  {
  	token=this.storage.get('accessToken')
  	 // this.isAuthenticate=this.RegisterService.auth()
  		// if(this.isAuthenticate==true)
  		// {
  		// 	return true;
  		// }
  		// else
  		// {
  		// 	window.alert("you have no right to access please login first");
  		// 	return false;
  		// }
      console.log(token)
      if (token===undefined) {
       
         this.toastr.error('You are not authorized please login');
         this.router.navigate(['']);
        return false;
    
      }
      else
      {
         
       return true;
      }



  }
  
}
