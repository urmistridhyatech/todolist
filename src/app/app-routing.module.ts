import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component';
import { RegisterComponent } from './register/register.component';
import { TasklistComponent } from './tasklist/tasklist.component';
import { ParenttaskComponent } from './parenttask/parenttask.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

const routes: Routes = [
 
 {path: '', component: LoginComponent },
 {path: 'register',  component: RegisterComponent },
 {path: '',  component: ParenttaskComponent,
  children: [
     { path: 'tasks', loadChildren: () => import('./tasks/tasks.module').then(m => m.TasksModule) },
 ]},
{path: 'logout',  component: LogoutComponent },
{path: '**',  component: PagenotfoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { 


}
