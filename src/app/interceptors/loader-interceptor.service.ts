import { Injectable,Injector,Inject} from "@angular/core";
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest,HttpResponse} from "@angular/common/http";
import { Observable} from "rxjs";
import { finalize } from "rxjs/operators";
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';


let token;
@Injectable()
export class LoaderInterceptor implements HttpInterceptor {
    constructor(private injector:Injector,@Inject(LOCAL_STORAGE) private storage:StorageService)
{}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        token=this.storage.get('accessToken');
      
// //     const secureReq = req.clone({
// //   url: req.url.replace('http://', 'https://')
// // });
//         this.loaderService.show();
//         return next.handle(req).pipe(
//             finalize(() => this.loaderService.hide())
//         );
        //return next.handle(secureReq);
        // let registeration=this.injector.get(RegisterationService);
        //  const authToken = registeration.getAuthorizationToken();

    // Clone the request and replace the original headers with
    // cloned headers, updated with the authorization.
    const authReq = req.clone({
      headers: req.headers.set('Authorization','Bearer '  +token)
    });
    
return next.handle(authReq);
    }
}