import { Component, OnInit,ChangeDetectionStrategy, Input, Output, EventEmitter} from '@angular/core';
import {TaskService} from '../task.service';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
// import * as $ from 'jquery';
// declare var $;


@Component({
  selector: 'app-tasklist',
  templateUrl: './tasklist.component.html',
  styleUrls: ['./tasklist.component.css']
})
export class TasklistComponent implements OnInit {
tasks:any[];
message:string="";


edittask={
  "task_name":"",
  };

id:number;
task_name:string="";
iseditform:string="";
p: number = 1;
status:boolean; 
  constructor(private TaskService:TaskService,private toastr: ToastrService,private router:Router) { }

  ngOnInit(): void {
 this.getTask();
 }
  getTask(): void
  {
        this.TaskService.getTasks()
      .subscribe(tasks => this.tasks= tasks['data']);
    }
onEdit(task)
{
this.iseditform="true";
this.task_name=task.task_name;
this.id=task.id;
}
onUpdate(id)
{
    this.getTask();  
    if(this.task_name=="")
    {
        this.message="Empty field is not allowed"
    }
    else{
     // this.TaskService.updateTask(this.title,id);
     // this.toastr.success('Succussfully Updated');
      this.TaskService.updateTask(this.task_name,id)
          .subscribe(
            data=> this.getTask(),
            );
   
   }
   this.getTask();
 // this.tasks=this.TaskService.getTasks();
 }
onDelete(tasks)
{
/* code for localstorage
  this.TaskService.deleteTask(id)
     this.toastr.success('Succussfully deleted');
 this.tasks=this.TaskService.getTasks();
 this.TaskService.getTasks();
*/
    this.TaskService.deleteTask(tasks.id)
        .subscribe(data=>this.getTask());
       this.toastr.success('task deleted');
     
}

//updates the task staus to done
updateStatus(tasks)
{
 this.TaskService.updateStatus(tasks)
      .subscribe(
        data=>this.getTask()
        );
  this.toastr.success('Status updated');
 }
}
