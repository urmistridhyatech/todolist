import { Injectable,Inject} from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
import { Observable, of } from 'rxjs';
import { HttpClient,HttpParams } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
let registerdata="registerdata";
let data=[];
let isAuthenticate=false;
let isEmailExist=false;
let isLoged="isLoged"
let success;
let token;
let accessToken="accessToken";
@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(@Inject(LOCAL_STORAGE) private storage:StorageService,private toastr: ToastrService,private router:Router,private http: HttpClient) { }
message:string="";

private loginUrl="https://php2.shaligraminfotech.com/todo_management/public/api/login";
private logoutUrl="https://php2.shaligraminfotech.com/todo_management/public/api/logout";

  register(registerData):boolean
  {

   		 	const currentTodoList = this.storage.get(registerdata) || [];

          data=this.storage.get(registerdata);

          for(let i = 0; i < data.length; i++)
          {
          if(data[i]['email']==registerData.email)
          {
            isEmailExist=true;
            return isEmailExist
          }
          
          else
          {
            
           isEmailExist=false;
          }
         }
          currentTodoList.push({
              firstname:registerData.firstname,
              lastname:registerData.lastname,
              email: registerData.email,
              
              password: registerData.password
          });
          this.storage.set(registerdata, currentTodoList);

         return isEmailExist;
          	
  }
  /*
    local storgae login 
  login(loginData):any
  {
            data=this.storage.get(registerdata);
            for(let i = 0; i < data.length; i++)
         	{
         	if(data[i]['email']==loginData.email && data[i]['password']==loginData.password)
         	{
         		this.storage.set(isLoged,true);
            this.storage.set(registerdata,data);
            }
        }
         this.authCheck();
        
  }*/
   authCheck():boolean
  {
      
        return (this.storage.get(isLoged));
   
  }

  login(loginData)
  {
   return this.http.post<any>(this.loginUrl,loginData)
    }
  authChecking(data)
  {
    this.message=data['messaage'];
    success=data['success'];
    token=data['accessToken'];
    if(data['success']==true)
    {
    this.toastr.success('Succussfully logged In');
    this.router.navigate(['tasks']); 
    this.storage.set(accessToken,data['data']['accessToken']);
  }
  else{
  this.toastr.error('please try again');
}
      this.auth();
    }
  responseMsg()
  {
    return (this.message)
  }
  auth():boolean
  {
    console.log(success);
    return (success);
  }
 
  logout()
  {
    this.storage.remove('accessToken');
   return this.http.post<any>(this.logoutUrl,"");

   
  }
}
