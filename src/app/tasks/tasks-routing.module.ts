import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth.guard';
import { TasklistComponent } from '../tasklist/tasklist.component';
import { PagenotfoundComponent } from '../pagenotfound/pagenotfound.component';
import { TasksComponent } from './tasks.component';



const routes: Routes = [{ path: '', component: TasklistComponent,canActivate:[AuthGuard] },
{ path: 'newtask', component: TasksComponent,canActivate:[AuthGuard] },
{path: '**',  component: PagenotfoundComponent },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TasksRoutingModule { }
