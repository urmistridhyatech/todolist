import { Component, OnInit } from '@angular/core';
import {TaskService} from '../task.service';
import { ToastrService } from 'ngx-toastr';
import {Router} from '@angular/router';
@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
	taskModel={
 task_name:""
 };
  constructor(private TaskService:TaskService,private toastr: ToastrService,private router:Router) { }

  ngOnInit(): void {
   this.TaskService.getTasks();
  }
  
	onSubmit()
  		{
   this.TaskService.newTask(this.taskModel)
     .subscribe(
        data=>this.TaskService.taskDetails(),
);
 	}
 }
