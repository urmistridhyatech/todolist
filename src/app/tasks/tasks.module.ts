import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AngularPaginatorModule } from 'angular-paginator';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import { AuthGuard } from '../auth.guard';

import { TasksRoutingModule } from './tasks-routing.module';
import { TasksComponent } from './tasks.component';

@NgModule({
  declarations: [TasksComponent],
  imports: [
    CommonModule,
    TasksRoutingModule,
    FormsModule,
    AngularPaginatorModule,
     NgxPaginationModule
  ],
   providers: [AuthGuard
  ],
})
export class TasksModule { }
